#include <stdio.h>
#include "cajero.h"
int main(int argc, char const *argv[]) {
  int seleccion=1;
  while (seleccion) {
    seleccion=menu();
    switch (seleccion) {
      case 1:
      Depositar();
      break;
      case 2:
      Retirar();
      break;
      case 3:
      Donar();
      break;
      case 0:
      printf("Saliendo...\n");
      break;
      default:
      printf("Esa opcion no existe\n");
    }
  }
  return 0;
}
